function quatiWorker() {
  // Utility print:
  function printInMain(message) {
      postMessage({ message: message });
  }

  // This needs to be established in order to debug errors:
  onerror = err => {
    throw new Error(err);
  };

  // So far, so good...
  printInMain("Worker loaded");

  // Wait to be informed about the placementUuid:
  onmessage = async function(event) {
    if (!("placement_uuid" in event.data)) {
      printInMain("Unexpected: " + event);
      return;
    }

    // All good to go!
    printInMain("Worker started");

    // Set placement uuid:
    const placementUuid = event.data["placement_uuid"];

    // Do a XHR request to download the WASM file:
    const allocationResponse = await fetch(
        "https://quati.systems/allocation?puuid=" + placementUuid
    );
    const allocation = await allocationResponse.json();

    // Test success:
    if (allocation["error"] !== undefined) {
      printInMain("Timed out");
      return;
    }

    // Outch!! Load module...
    eval(allocation["unit-js"]);

    printInMain("Allocation loaded");

    // Init unit:
    const cancel = Rust.run_unit({ app_url: allocation["app"] });

    printInMain("Unit started");

    // Define what happens when the socket is ordered to close cleanly:
    onmessage = event => {
      if ("quati_unload" in event.data) {
        printInMain("Unload event");
        cancel();
      } else {
        printInMain("Unexpected: " + event);
        cancel.drop();
      }
    };
  };
}

// Get placement uuid for this script:
const placement_uuid = document.currentScript.getAttribute("puuid");

// Do nothing if there is no placement_uuid:
if (placement_uuid === null) {
  console.log("Ooops! Placement uuid not defined.");
} else {
  const source = new Blob(["(", quatiWorker.toString(), ")()"], {
    type: "text/javascript"
  });

  const worker = new Worker(URL.createObjectURL(source));

  worker.onmessage = evnt => {
    if ("message" in evnt.data) {
      // Log stuff...
      console.log(evnt.data["message"]);
    } else {
      // For other stuff...
      console.log("Unexpected: ");
      console.log(evnt);
    }
  };

  // Kick off worker sending placement_uuid:
  worker.postMessage({ placement_uuid: placement_uuid });

  // Define what happens when the window closes:
  window.addEventListener("beforeunload", evnt => {
    if (worker) {
      worker.postMessage({ quati_unload: null });
    }
  });
}
