import asyncio
import websockets

async def echo(websocket, path):
    while True:
        message = await websocket.recv()
        print(message)
        await websocket.send(message)

asyncio.get_event_loop().run_until_complete(
    websockets.serve(echo, 'localhost', 8081))
asyncio.get_event_loop().run_forever()
