#![feature(async_await, await_macro, futures_api, pin)]
#![feature(try_blocks)]

#[macro_use]
mod utils;

pub mod controller;
pub mod ffi;

use serde_derive::{Deserialize, Serialize};
use std::cell::RefCell;
use std::rc::Rc;
use stdweb::web::event::IMessageEvent;
use stdweb::web::*;
use stdweb::{__js_deserializable_serde_boilerplate, js_deserializable};

use fish_core::Unit;

use crate::controller::Controller;

#[derive(Debug)]
pub enum RuntimeError {
    WsCreationError,
    ToArrayBufferError,
    ToVecError,
    Controller(controller::Error),
    WsSendError,
}

/// Defines the environment for a unit run.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UnitEnv {
    app_url: String,
}

js_deserializable!(UnitEnv);

impl UnitEnv {
    pub fn run<U: Unit>(self) -> Result<Handle<U>, RuntimeError> {
        // Init controller and socket:
        let controller = Rc::new(RefCell::new(Controller::<U>::new()));
        let socket = WebSocket::new(&self.app_url).map_err(|_| RuntimeError::WsCreationError)?;

        // Define protocol data type (binary data):
        socket.set_binary_type(SocketBinaryType::ArrayBuffer);

        // Message handler:
        socket.add_event_listener(
            enclose!((socket, controller) move |e: event::SocketMessageEvent| {
                let react_to: Result<(), RuntimeError> = try {
                    let data: Vec<u8> = e
                        .data()
                        .into_array_buffer()
                        .ok_or(RuntimeError::ToArrayBufferError)?
                        .into();

                    let message = bincode::deserialize(&data);

                    let controller_state = controller
                        .borrow_mut()
                        .receive(message)
                        .map_err(|err| RuntimeError::Controller(err))?;

                    if let Some(from_client) = controller
                        .borrow_mut()
                        .send(controller_state)
                        .map_err(|err| RuntimeError::Controller(err))?
                    {
                        socket
                            .send_bytes(&bincode::serialize(&from_client).unwrap())
                            .map_err(|_| RuntimeError::WsSendError)?;
                    }
                };

                if let Err(error) = react_to {
                    printjs!("{:?}", error);
                }
            }),
        );

        // When an error happens _in the protocol_:
        socket.add_event_listener(|_: event::SocketErrorEvent| {
            printjs!("Error in WebSocket");
        });

        Ok(Handle { socket, controller })
    }
}

pub struct Handle<U: Unit> {
    socket: WebSocket,
    controller: Rc<RefCell<Controller<U>>>,
}

impl<U: Unit> Handle<U> {
    pub fn cancel(self) {
        let final_words = self.controller.borrow_mut().close();

        self.socket
            .send_bytes(&bincode::serialize(&final_words).unwrap())
            .map_err(|_| RuntimeError::WsSendError)
            .map_err(|err| printjs!("{:?}", err))
            .ok();

        self.socket.close();
    }
}

// fn main() {
//     let app_url = "ws://localhost:8081";
//     let socket = WebSocket::new(app_url).unwrap();
//     socket.set_binary_type(SocketBinaryType::ArrayBuffer);
//
//     // let controller = Controller::new();
//     socket.add_event_listener(move |e: event::SocketMessageEvent| {
//         let data: Vec<u8> = e.data().into_array_buffer().unwrap().into();
//         printjs!("{:?}", data);
//     });
//
//     let socket_ = socket.clone();
//     socket.add_event_listener(move |_: event::SocketOpenEvent| {
//         socket_.send_bytes(&vec![1, 2, 3, 4, 5, 6, 7, 8]).unwrap();
//     });
// }
