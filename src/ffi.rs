#[doc(hidden)]
pub fn print_gambiarra<T: std::fmt::Debug>(t: &T) {
    printjs!("{:?}", t);
}

#[macro_export]
macro_rules! fish_export {
    ($unit:ty) => {
        use stdweb::Once;
        use stdweb::{__js_raw_asm, js_export};
        use $crate::{ffi::print_gambiarra, UnitEnv};

        #[js_export]
        pub fn run_unit(unit_env: UnitEnv) -> Once<impl FnOnce()> {
            // Start unit and get handle:
            let handle = unit_env.run::<$unit>();

            // Print errors:
            if let Err(err) = handle.as_ref() {
                print_gambiarra(err);
            }

            // Handler to cancel stuff:
            Once(|| {
                if let Ok(handle) = handle {
                    handle.cancel();
                }
            })
        }
    };
}

// mod test {
//     use fish_core::Unit;
//     use serde_derive::{Deserialize, Serialize};
//
//     #[derive(Clone, Debug, Serialize, Deserialize)]
//     pub struct Dummy;
//
//     #[derive(Default, Debug, Clone, Serialize, Deserialize)]
//     pub struct DummyState;
//
//     impl Unit for Dummy {
//         type State = DummyState;
//         type Question = ();
//         type Answer = ();
//         fn run(&self, _: &mut Self::State, _: Self::Question) -> Self::Answer {}
//     }
//
//     fish_export!(Dummy);
// }
