use fish_core::message::{ExecType, FromClient, FromServer};
use fish_core::{State, Unit};

pub enum ControllerState<U: Unit> {
    NotLoaded(u64),
    Loaded,
    AlreadyLoaded(Option<U>, Option<U::State>),
    LoadFailed(Option<U>, Option<U::State>, String),
    Running(u64, U::Question),
    ServerDecodeError(String),
    ClientDecodeError(String),
    Retrieving,
    Ending,
}

use self::ControllerState::*;

/// These are all abnormal errors. They are ipemented to aid debugging.
#[derive(Debug)]
pub enum Error {
    GotServerDecodeError(String),
    Serialization(bincode::Error),
    PoisonedAtRead,
    PoisonedAtWrite,
    UnitNotThere,
    StateNotThere,
}

use self::Error::*;

pub struct Controller<U: Unit> {
    exec_type: ExecType,
    unit: Option<U>,
    state: Option<U::State>, // State set to `None` means "not loaded".
}

impl<U: Unit> Default for Controller<U> {
    fn default() -> Self {
        Self::new()
    }
}

impl<U: Unit> Controller<U> {
    pub fn new() -> Controller<U> {
        Controller {
            exec_type: ExecType::Retrieving, // To be overridden.
            unit: None,
            state: None,
        }
    }

    pub fn receive(
        &mut self,
        message: Result<FromServer<U>, bincode::Error>,
    ) -> Result<ControllerState<U>, Error> {
        Ok(match (self.state.as_ref(), message) {
            // When not loaded (or load failed) and got a unit, load!
            (
                None,
                Ok(FromServer::Unit {
                    unit,
                    maybe_state,
                    exec_type,
                }),
            ) => {
                self.exec_type = exec_type;
                self.unit = Some(unit);
                self.state = Some(maybe_state.unwrap_or_else(U::State::init));

                Loaded
            }
            // Anything else, complain that controller is not available.
            (
                Some(_),
                Ok(FromServer::Unit {
                    unit,
                    maybe_state,
                    exec_type,
                }),
            ) => AlreadyLoaded(exec_type.decide(unit), maybe_state),
            // Anything that is not a unit will get a "not loaded" complaint.
            (None, Ok(FromServer::Question { oid, .. })) => NotLoaded(oid),
            (None, Ok(_)) => NotLoaded(0),
            // We've got a question. Run!
            (_, Ok(FromServer::Question { oid, question })) => Running(oid, question),
            // Got a server decode error. Cry!
            (_, Ok(FromServer::DecodeError { message: error })) => ServerDecodeError(error),
            // Stop running the unit and send it back.
            (_, Ok(FromServer::Retrieve)) => Retrieving,
            // Stop running the unit and never return the unit.
            (_, Ok(FromServer::End)) => Ending,
            // Non capisco. Could you repeat, please?
            (_, Err(error)) => ClientDecodeError(format!("{:?}", error)),
        })
    }

    pub fn send(
        &mut self,
        controller_state: ControllerState<U>,
    ) -> Result<Option<FromClient<U>>, Error> {
        Ok(match controller_state {
            // If we are not loaded and have to send something, complain!
            NotLoaded(oid) => Some(FromClient::NotLoaded { oid }),
            // If we are "already loaded", complain!
            AlreadyLoaded(maybe_unit, maybe_state) => Some(FromClient::LoadFailed {
                maybe_unit,
                maybe_state,
                message: "Already loaded.".to_string(),
            }),
            // If we have just loaded, spread the good news!
            Loaded => Some(FromClient::Loaded),
            // If load failed, communicate the bad news...
            LoadFailed(maybe_unit, maybe_state, error) => Some(FromClient::LoadFailed {
                maybe_unit,
                maybe_state,
                message: error,
            }),
            // If we have to run the question, run it!
            Running(oid, question) => {
                // Get state:
                let mut state = self.state.take().ok_or(StateNotThere)?.clone();

                // Run question:
                let answer = self
                    .unit
                    .as_ref()
                    .ok_or(UnitNotThere)?
                    .run(&mut state, question);

                // Update state:
                self.state = Some(state);

                // Serialize answer:
                Some(FromClient::Answer { oid, answer })
            }
            // Ops! Server error. Return with error.
            ServerDecodeError(error) => return Err(GotServerDecodeError(error)),
            // Non capisco.
            ClientDecodeError(error) => Some(FromClient::DecodeError {
                oid: 0,
                message: error,
            }),
            // Show's over.
            Retrieving => Some(FromClient::Retrieve {
                maybe_unit: self.unit.take().and_then(|unit| self.exec_type.decide(unit)),
                maybe_state: self.state.take(),
            }),
            // Shows's over and never bother about returning the unit.
            Ending => {
                self.state = None;
                None
            }
        })
    }

    /// Runs a receive-send cycle.
    pub fn close(&mut self) -> FromClient<U> {
        FromClient::Retrieve {
            maybe_unit: self.unit.take().and_then(|unit| self.exec_type.decide(unit)),
            maybe_state: self.state.take(),
        }
    }
}
