macro_rules! js {
    ($($arg:tt)*) => {{
        #[allow(unused_imports)]
        use stdweb::{_js_impl, __js_raw_asm};
        stdweb::js! { $($arg)* }
    }};
}

macro_rules! printjs {
    ($($arg:tt)*) => {{
        let message = format!($($arg)*);
        js! {
            if (window.document === undefined) {
                postMessage({message: @{&message}});
            } else {
                console.log(@{&message});
            }
        }
    }};
}

// Shamelessly stolen from webplatform's TodoMVC example.
macro_rules! enclose {
    ( ($( $x:ident ),*) $y:expr ) => {
        {
            $(let $x = $x.clone();)*
            $y
        }
    };
}
